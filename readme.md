## What is this?
Mortion is a simple prototype for a gaming community platform. This project has been written as an assignment for Digicreate.

The requirements of this assignment were the following:

- Users can like e-sport teams
- Users can like other users
- Updates of liked content should be displayed on the dashboard
- A team consists of multiple users
- An event can have multiple teams
- Comments can be added to events
- Design of the website can be simple
- A CSS framework can be used
- The database can be seeded with fake data

## How do I view the result?

- Clone this repository locally
- Update the .env file to match your database setup
- Migrate the database using:
```
php artisan migrate
```
- Seed the database using:
```
php artisan db:seed
```
- Use your favourite local server setup or serve the website using:
```
php artisan serve
```
- Open a browser and surf to the website. You can log in with test@mortion.be using "123456" as password. Or you can register your own account.

## Disclaimer
This is a simple website and provides only the basic functionalities needed to complete the assignment.
I am using random images from the Unslpash API, but I did not take the time to do this efficiently.
The deadline I gave myself was 6 hours so I did not focus on performance.
Also, some of the actions (liking content for example) could be done more elegantly using async JS,
I chose not to invest time in implementing this because it is just eye candy.
I would however take the time to do this in a production environment.