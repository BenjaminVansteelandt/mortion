
/**
 * First we will load all of this project's JavaScript dependencies which
 * includes Vue and other libraries. It is a great starting point when
 * building robust, powerful web applications using Vue and Laravel.
 */

require('./bootstrap');

$(document).ready(init);

var globals = {
	isUserDropdownShown: false
};

function init() {
	// Carousel
	showCarousel();

	// User profile
	$('.user-avatar').click(showUserDropdown);

	// Teams
	$('#join-team').click(joinTeam);

	// Likes
	$('#like-user').click(likeUser);
	$('#like-team').click(likeTeam);
}

function showCarousel() {
	$('.carousel').show(2000);
}

function ajaxPost(url, data) {
	data._token = $("meta[name='csrf-token']").attr("content");

	console.log(url, data);

	$.ajax({
		url: url,
		method: 'POST',
		data: data,
		success: function() { location.reload(); },
		error: function(e) { console.log(e); $('.error').html(e.responseText); }
	})
}

function showUserDropdown() {
	var $dropdown = $('.user-dropdown');

	if(!globals.isUserDropdownShown) {
		$dropdown.slideDown(500);
	} else {
		$dropdown.hide(500);
	}

	globals.isUserDropdownShown = !globals.isUserDropdownShown;
}

function joinTeam(e) {
	e.preventDefault();

	var userID = $(e.target).attr('data-user');
	var teamID = $(e.target).attr('data-team');

	var data = {
		'user_id': userID,
		'team_id': teamID
	};

	ajaxPost('/teams/users/add', data);
}

function likeUser(e) {
	e.preventDefault();

	var url = '/users/like';
	var userID = $(e.target).attr('data-from-user');
	var toUserID = $(e.target).attr('data-to-user');

	var data = {
		'from_user_id': userID,
		'to_user_id': toUserID,
	};

	ajaxPost(url, data);
}

function likeTeam(e) {
	e.preventDefault();
	var url = '/teams/like';
	var userID = $(e.target).attr('data-user');
	var toUserID = $(e.target).attr('data-team');

	var data = {
		'user_id': userID,
		'team_id': toUserID,
	};
console.log(data);
	ajaxPost(url, data);
}


