@extends('layouts.master')

@section('content')

	<h1>Create a new team</h1>

	{!! Form::open(array('url'=>action('TeamsController@store'))) !!}
	<input type="hidden" name="_token" value="{{ csrf_token() }}">

	<div class="form-group">
		{!! Form::label('name', "The name makes the team like the clothes make the man") !!}
		{!! Form::text('name', '', array('class' => 'form-control')) !!}
	</div>

	<div class="form-group">
		{!! Form::submit('Save me', array('class' => 'btn btn-primary')) !!}
	</div>

@endsection