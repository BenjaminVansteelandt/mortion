@extends('layouts.master')

@section('content')

	<h1>Teams</h1>

	<div class="actions">
		<ul>
			<li><a href="{{ action('TeamsController@create') }}">Create a team</a></li>
		</ul>
	</div>

	<div class="teams-list-wrapper">
		@if(count($teams) == 0)
			<p>There are no teams yet. Are you brave enough to <a href="{{ action('TeamsController@create') }}">create</a> the first one?</p>
		@else
			@foreach($teams as $team)
				<div class="team">
					<div class="team-name">
						<a href="{{ action('TeamsController@show', $team->id) }}">{{ $team->name }}</a>
					</div>
				</div>
			@endforeach
		@endif
	</div>

@endsection