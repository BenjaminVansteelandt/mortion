@extends('layouts.master')

@section('content')

	<h1>{{ $team->name }}</h1>

	<div class="actions">
		<ul>
			<li>
				@if($can_like)
					<a href="#" id="like-team" data-team="{{ $team->id }}" data-user="{{ \Illuminate\Support\Facades\Auth::user()->id }}" ><i class="glyphicon glyphicon-thumbs-up"></i> {{ $likes }}</a>
				@else
					<i class="glyphicon glyphicon-thumbs-up"></i> {{ $likes }}
				@endif
			</li>
			@if($can_join)
				<li>
					<a href="#" id="join-team" data-team="{{ $team->id }}" data-user="{{ \Illuminate\Support\Facades\Auth::user()->id }}">Join this team</a>
				</li>
			@endif
		</ul>
	</div>

	@if(count($members) == 0)
		<p>Be the first one to <a href="#">join this team</a></p>
	@else
		<div class="members">
			<h3> {{ count($members) }} <?= count($members) > 1 ? 'members' : 'member' ?></h3>
			@foreach($members as $member)
				<div class="member">
					<a href="{{ action('UsersController@show', $member->user->id) }}">
						<div class="member-avatar"><img src="https://unsplash.it/80/80?random" alt="{{ $member->user->name }}"></div>
						<div class="member-name">{{ $member->user->name }}</div>
					</a>
				</div>
			@endforeach
		</div>
	@endif
@endsection