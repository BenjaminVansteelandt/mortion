<!DOCTYPE html>
<html lang="{{ config('app.locale') }}">
<head>
	<meta charset="utf-8">
	<meta http-equiv="X-UA-Compatible" content="IE=edge">
	<meta name="viewport" content="width=device-width, initial-scale=1">

	<!-- CSRF Token -->
	<meta name="csrf-token" content="{{ csrf_token() }}">

	<title>{{ config('app.name', 'Mortion') }}</title>

	<!-- Styles -->
	<link href="https://fonts.googleapis.com/css?family=Rock+Salt" rel="stylesheet">
	<link href="{{ asset('css/app.css') }}" rel="stylesheet">
</head>
<body>

<?php use Illuminate\Support\Facades\Auth; $current_user = Auth::user(); ?>

@include('includes.header')
<div class="row content-wrapper">
	<div class="col-sm-2">
		@include('includes.nav-side')
	</div>

	<div class="col-sm-10">
		@yield('content')
	</div>
</div>

<div class="error"></div>

@include('includes.footer')

<script src="{{ asset('js/app.js') }}"></script>

</body>
</html>