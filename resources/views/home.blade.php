@extends('layouts.master')

@section('content')
    <div class="col-md-9">
        <div id="myCarousel" class="carousel slide" data-ride="carousel">
            <!-- Indicators -->
            <ol class="carousel-indicators">
                <li data-target="#myCarousel" data-slide-to="0" class="active"></li>
                @for($i = 1; $i<3; $i++)
                    <li data-target="#myCarousel" data-slide-to="{{ $i }}"></li>
                @endfor
            </ol>

            <!-- Wrapper for slides -->
            <div class="carousel-inner">
                <div class="item active">
                    <img src="https://unsplash.it/1280/680?random" alt="Los Angeles">
                    <div class="carousel-caption">
                        <h3>Lorem ipsum</h3>
                        <p>dolor sit amet, consectetur adipiscing elit</p>
                    </div>
                </div>

                @for($i = 0; $i < 2; $i++)
                    <div class="item">
                        <img src="https://unsplash.it/1280/680?image={{ $i + 40 }}" alt="Chicago">
                        <div class="carousel-caption">
                            <h3>Lorem ipsum</h3>
                            <p>dolor sit amet, consectetur adipiscing elit</p>
                        </div>
                    </div>
                @endfor
            </div>

            <!-- Left and right controls -->
            <a class="left carousel-control" href="#myCarousel" data-slide="prev">
                <span class="glyphicon glyphicon-chevron-left"></span>
                <span class="sr-only">Previous</span>
            </a>
            <a class="right carousel-control" href="#myCarousel" data-slide="next">
                <span class="glyphicon glyphicon-chevron-right"></span>
                <span class="sr-only">Next</span>
            </a>
        </div>

        <div class="home-main-wrapper">
            @for($i = 0; $i < 3; $i++)
                <div class="news-item">
                    <h3>Lorem ipsum</h3>
                    <p>dolor sit amet, consectetur adipiscing elit. Suspendisse pharetra lectus non nulla tristique, nec molestie ante tempus. Quisque sit amet orci ac nunc sagittis ultrices non sed massa. Vivamus augue neque, porta et feugiat ut, vestibulum id diam. Pellentesque suscipit felis tellus, vel ornare justo malesuada vel. Nullam scelerisque eu metus sit amet tempor. Morbi feugiat, elit at consequat aliquam, ipsum metus egestas quam, ut porta ligula odio in quam. Nullam dictum odio ultricies felis porta, eget egestas erat maximus. Donec vel odio est. Duis nulla urna, sodales non velit quis, bibendum mattis libero. Pellentesque ac dictum magna. Nunc rhoncus dictum neque eu lobortis. Maecenas vulputate velit nibh, nec mollis felis imperdiet vitae. Maecenas leo quam, auctor maximus ipsum id, congue facilisis urna.</p>
                    <p class="text-right">{{ \Carbon\Carbon::now()->addDays(-$i)->diffForHumans(\Carbon\Carbon::now()) }}</p>
                </div>
            @endfor
        </div>
    </div>

    <div class="col-md-3">
        <div class="home-news-wrapper">
            @for($i = 0; $i < 3; $i++)
                <div class="news-item">
                    <div class="news-item-title">
                        News item title {{ $i }}
                    </div>
                    <div class="news-item-content">
                        Lorem ipsum dolor sit amet, consectetur adipiscing elit. Suspendisse pharetra lectus non nulla tristique, nec molestie ante tempus. Quisque sit amet orci ac nunc sagittis ultrices non sed massa. Vivamus augue neque, porta et feugiat ut, vestibulum id diam. Pellentesque suscipit felis tellus, vel ornare justo malesuada vel.
                    </div>
                </div>
            @endfor
        </div>
    </div>
@endsection
