@extends('layouts.master')

@section('content')
	<div class="user-profile">
		<div class="user-avatar user-placeholder"></div>
		<h1>{{ $user->name }}</h1>

		<div class="actions">
			<ul>
				@if(!$is_current_user && $can_like)
					<li>
						<a href="#" id="like-user" data-to-user="{{ $user->id }}" data-from-user="{{ \Illuminate\Support\Facades\Auth::user()->id }}">
							<i class="glyphicon glyphicon-thumbs-up"></i> {{ $likes }}
						</a>
					</li>
				@else
					<li>
						<i class="glyphicon glyphicon-thumbs-up"></i> {{ $likes }}
					</li>
				@endif
			</ul>
		</div>

		<div class="user-details-wrapper">
			<h3>User information</h3>
			<ul>
				<li>Email: {{ $user->email }}</li>
				<li>Location: Belgium</li>
				<li>...</li>
			</ul>
		</div>

		<div class="user-teams-wrapper">
			<h3>Teams</h3>
			@if(count($teams) > 0)
				@foreach($teams as $team)
					<div class="team">
						<a href="/teams/show/{{ $team->team->id }}">
							<div class="team-avatar"><img src="https://unsplash.it/50/50?random" alt="{{ $team->team->name }}"></div>
							<div class="team-name">{{ $team->team->name }}</div>
						</a>
					</div>
				@endforeach
			@else
				<p>{{ $user->name }} hasn't signed joined any team yet.</p>
			@endif
		</div>
	</div>
@endsection