@extends('layouts.master')

@section('content')

	<h1>Create an event</h1>

	{!! Form::open( array( 'url' => action('EventsController@store'))) !!}

	<input type="hidden" name="_token" value="{{ csrf_token() }}">

	<div class="form-group">
		{!! Form::label('name', "What's an event without a title?") !!}
		{!! Form::text('name', '', array('class' => 'form-control')) !!}
	</div>

	<div class="form-group">
		{!! Form::label('description', "Tell the people what is going to happen") !!}
		{!! Form::textarea('description', '', array('class' => 'form-control')) !!}
	</div>

	<div class="form-group">
		{!! Form::label('time', "And when it's going to happen") !!}
		<input type="datetime-local" name="time" class="form-control"/>
	</div>

	<div class="form-group">
		{!! Form::submit('Save me', array('class' => 'btn btn-primary')) !!}
	</div>

	{!! Form::close() !!}

@endsection