@extends('layouts.master')

@section('content')


	<div class="event-wrapper">
		<div class="event">
			<div class="event-title">
				<h2>{{ $event->name }}</h2>
				<div class="event-time">{{ $event->time }}</div>
			</div>

			<div class="event-description">
				<div class="col-md-9">
					{{ $event->description }}
				</div>

				<div class="col-md-3">
					@if(count($teams) == 0)
						<p>No teams have signed up for this event yet.</p>
					@else
						@foreach($teams as $team)
							<div class="event-team">
								<div class="team user-placeholder"></div>
							</div>
						@endforeach
					@endif
				</div>
			</div>
		</div>

		<div class="event-comment-wrapper">

			<h3>Comments</h3>

			@if(count($comments) == 0)
				<div class="comment">
					<p>Be the first to comment something!</p>
				</div>
			@else
				@foreach($comments as $comment)
					<div class="row comment">
						<div class="col-sm-1">
							<a href="{{ action('UsersController@show', $comment->user->id) }}">
								<div class="author-avatar user-placeholder"></div>
							</a>
						</div>
						<div class="col-sm-11">
							<div class="author">
								<a href="{{ action('UsersController@show', $comment->user->id) }}">
									{{ $comment->user->name }}
								</a>
							</div>
							<div class="comment-body">{{ $comment->comment }}</div>
						</div>
					</div>
				@endforeach
			@endif

			@if(Auth::user())


				{!! Form::open(array('url'=>action('EventsController@addComment'))) !!}

				<input type="hidden" name="_token" value="{{ csrf_token() }}">
				<input type="hidden" name="user_id" value="{{ Auth::user()->id }}"/>
				<input type="hidden" name="event_id" value="{{ $event->id }}"/>

				<div class="form-group">
					{!! Form::label('comment', 'Thoughts? Shoot!') !!}
					{!! Form::textarea('comment', '', array('class'=>'form-control')) !!}
				</div>

				<div class="form-group">
					{!! Form::submit('Save me', array('class' => 'btn btn-primary')) !!}
				</div>

				{!! Form::close() !!}


			@endif

		</div>
	</div>

@endsection