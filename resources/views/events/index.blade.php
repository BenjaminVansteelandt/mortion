@extends('layouts.master')

@section('content')

	<h1>Events</h1>

	<div class="actions">
		<ul>
			<li>
				<a href="{{ action('EventsController@create') }}">Create an event</a>
			</li>
		</ul>
	</div>

	<div class="event-list-wrapper">
		@foreach($events as $event)
			<div class="event">
				<div class="event-title">
					<a href="{{ action('EventsController@show', $event->id) }}">{{ $event->name }}</a>
				</div>

				<div class="event-date">
					{{ $event->time }}
				</div>
			</div>
		@endforeach
	</div>

@endsection