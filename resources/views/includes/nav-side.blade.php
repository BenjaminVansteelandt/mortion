
<?php
	use Illuminate\Support\Facades\Request;
?>

<div class="row">
	<div class="sidebar-nav text-right">
		<div class="navbar navbar-default" role="navigation">
			<div class="navbar-header">
				<button type="button" class="navbar-toggle" data-toggle="collapse" data-target=".sidebar-navbar-collapse">
					<span class="sr-only">Toggle navigation</span>
					<span class="icon-bar"></span>
					<span class="icon-bar"></span>
					<span class="icon-bar"></span>
				</button>
				<span class="visible-xs navbar-brand">Menu</span>
			</div>

			<div class="navbar-collapse collapse sidebar-navbar-collapse">
				<ul class="nav navbar-nav">
					<li {{ Request::is('home') || Request::is('/') ? 'class=active' : '' }}><a href="{{ action('HomeController@index') }}">Home</a></li>
					<li {{ Request::is('events') ? 'class=active' : '' }}><a href="{{ action('EventsController@index') }}">Events</a></li>
					<li {{ Request::is('teams') ? 'class=active' : '' }}><a href="{{ action('TeamsController@index') }}">Teams</a></li>
				</ul>
			</div>
		</div>
	</div>
</div>

