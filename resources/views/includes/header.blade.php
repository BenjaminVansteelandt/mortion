<header>
	<div class="row">
		<div class="col-md-12">

			<div class="logo text-center">
				<a href="/">Mortion</a>
			</div>

			@if(isset($current_user))
				<div class="pull-right">
					<div class="user-control">

						<div class="user-avatar user-placeholder"></div>

						<div class="user-dropdown">
							<div class="fancy-border"></div>

							<ul>
								<li>
									<a href="{{ action('UsersController@show', $current_user->id) }}"><i class="glyphicon glyphicon-user"></i> Profile</a>
								</li>
								<li>
									<a href="{{ url('/logout') }}"  onclick="event.preventDefault();   document.getElementById('logout-form').submit();">
										<i class="glyphicon glyphicon-log-out"></i> Logout
									</a>

									<form id="logout-form"  action="{{ url('/logout') }}"  method="POST"  style="display: none;">
										{{ csrf_field() }}
									</form>
								</li>
							</ul>
						</div>

					</div>
				</div>
			@endif

		</div>
	</div>
</header>