<?php
	
	/*
	|--------------------------------------------------------------------------
	| Web Routes
	|--------------------------------------------------------------------------
	|
	| Here is where you can register web routes for your application. These
	| routes are loaded by the RouteServiceProvider within a group which
	| contains the "web" middleware group. Now create something great!
	|
	*/
	
	Auth::routes();
	
	/* Home */
	/* ------------------------------------------------------------------------------------------- */
	
	Route::get('/', 'HomeController@index');
	Route::get('home', 'HomeController@index');
	
	/* Users */
	/* ------------------------------------------------------------------------------------------- */
	
	Route::get('users/profile/{id}', 'UsersController@show');
	
	Route::post('users/create', 'UsersController@create');
	
	/* Teams */
	/* ------------------------------------------------------------------------------------------- */
	
	Route::get('teams', 'TeamsController@index');
	Route::get('teams/show/{id}', 'TeamsController@show');
	Route::get('teams/create', 'TeamsController@create');
	
	Route::post('teams/store', 'TeamsController@store');
	Route::post('teams/users/add', 'TeamsController@addUser');
	Route::post('teams/users/remove', 'TeamsController@removeUser');
	
	/* Events */
	/* ------------------------------------------------------------------------------------------- */
	
	Route::get('events', 'EventsController@index');
	Route::get('events/show/{id}', 'EventsController@show');
	Route::get('events/create', 'EventsController@create');
	
	Route::post('events/store', 'EventsController@store');
	Route::post('events/teams/add', 'EventsController@addTeam');
	Route::post('events/teams/remove', 'EventsController@removeTeam');
	Route::post('events/comments/add', 'EventsController@addComment');
	
	/* Likes */
	/* ------------------------------------------------------------------------------------------- */
	
	Route::post('users/like', 'LikesController@likeUser');
	Route::post('teams/like', 'LikesController@likeTeam');
	
	/* Roles */
	/* ------------------------------------------------------------------------------------------- */
	// TODO: Implement
