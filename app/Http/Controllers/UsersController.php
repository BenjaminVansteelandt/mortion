<?php

namespace App\Http\Controllers;

use App\TeamUser;
use App\User;
use App\UserLike;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

class UsersController extends Controller
{
    public function create(Request $request) {
	    $data = $request->all();
	    $data["password"] = bcrypt($data["password"]);
	    
	    User::create($data);
	    
	    return redirect()->action('HomeController@index');
    }
    
    public function show($id) {
	    $current_user_id = Auth::user()->id;
	    $user = User::findOrFail($id);
	    $teams = TeamUser::where('user_id', $id)->get();
	    $likes = UserLike::where('to_user_id', $id)->get();

	    $is_current_user = $id == $current_user_id;
	    $can_like = true;
	    
	    foreach($likes as $like) {
		    if($like->to_user_id == $id && $like->from_user_id == $current_user_id) {
			    $can_like = false;
			    break;
		    }
	    }
	    
	    $likes = count($likes);
	    
	    return view('users.profile')
		    ->with('user', $user)
		    ->with('teams', $teams)
		    ->with('likes', $likes)
		    ->with('can_like', $can_like)
		    ->with('is_current_user', $is_current_user);
    }
}
