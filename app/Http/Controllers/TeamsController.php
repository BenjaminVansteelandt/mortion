<?php

namespace App\Http\Controllers;

use App\Team;
use App\TeamLike;
use App\TeamUser;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

class TeamsController extends Controller
{
    public function index() {
	    $teams = Team::all();
	    
	    return view('teams.index')->with('teams', $teams);
    }
    
    public function show($id) {
	    $team = Team::findOrFail($id);
	    $members = TeamUser::where('team_id', $id)->get();
	    $likes = TeamLike::where('team_id', $id)->get();
	    
	    $can_like = true;
	    $can_join = true;
	    
	    foreach ($likes as $like) {
		    if($like->user_id == Auth::user()->id) {
			    $can_like = false;
			    break;
		    }
	    }
	
	    foreach ($members as $member) {
		    if($member->id == Auth::user()->id) {
			    $can_join = false;
			    break;
		    }
	    }
	    
	    $likes = count($likes);
	    
	    return view('teams.show')
		    ->with('team', $team)
		    ->with('members', $members)
		    ->with('likes', $likes)
		    ->with('can_like', $can_like)
		    ->with('can_join', $can_join);
    }
    
    public function create() {
	    return view('teams.create');
    }
    
    public function store(Request $request) {
	    $data = $request->all();
	    $team = Team::create($data);
	    
	    return redirect()->action('TeamsController@show', $team->id);
    }
    
    public function addUser(Request $request) {
	    $data = $request->all();
	    
	    TeamUser::create(array(
	    	'user_id' => $data["user_id"],
		    'team_id' => $data["team_id"]
	    ));
	    
	    return redirect()->action('TeamsController@show', $data["team_id"]);
    }
    
    public function removeUser(Request $request) {
	    $data = $request->all();
	    
	    $member = TeamUser::where(array(
	    	'user_id' => $data["user_id"],
		    'team_id' => $data["team_id"]
	    ));
	    
	    $member->delete();
	    
	    return redirect()->back();
    }
}
