<?php

namespace App\Http\Controllers;

use App\Comment;
use App\Event;
use App\EventTeam;
use App\TeamUser;
use Illuminate\Http\Request;

class EventsController extends Controller
{
    public function index() {
	    $events = Event::all();
	    
	    return view('events.index')->with('events', $events);
    }
    
    public function show($id) {
	    $event = Event::findOrFail($id);
	    $comments = Comment::where('event_id', $id)->get();
	    $teams = EventTeam::where('event_id', $id)->get();
	    
	    return view('events.show')
		    ->with('event', $event)
		    ->with('comments', $comments)
		    ->with('teams', $teams);
    }
    
    public function create() {
	    
	    return view('events.create');
    }

    public function store(Request $request){
	    $data = $request->all();
	    
	    $event = Event::create($data);
	    
	    return redirect()->action('EventsController@show', $event->id);
    }
    
    public function addTeam(Request $request) {
	    $data = $request->all();
	    
	    EventTeam::create(array(
	    	'event_id' => $data["event_id"],
		    'team_id' => $data["team_id"]
	    ));
	    
	    return redirect()->action('EventsController@show', $data["team_id"]);
    }
    
    public function removeTeam(Request $request) {
	    $data = $request->all();
	    
	    $team = Event::findOrFail($data["team_id"]);
	    $team->delete();
	    
	    return redirect()->action('EventsController@show', $data["team_id"]);
    }
    
    public function addComment(Request $request) {
	    $data = $request->all();
	    
	    Comment::create(array(
	    	'comment' => $data["comment"],
		    'user_id' => $data["user_id"],
		    'event_id' => $data["event_id"]
	    ));
	    
	    return redirect()->back();
    }
}
