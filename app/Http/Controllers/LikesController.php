<?php

namespace App\Http\Controllers;

use App\Team;
use App\TeamLike;
use App\UserLike;
use Illuminate\Http\Request;

class LikesController extends Controller
{
    public function likeUser(Request $request) {
	    $data = $request->all();
	    
	    $from = $data["from_user_id"];
	    $to = $data["to_user_id"];
	    
	    $like = UserLike::where(array(
	    	'from_user_id' => $from,
		    'to_user_id' => $to
	    ))->get();
	    
	    if(count($like) > 0) {
		    throw new \Exception("You can't like a user twice");
	    }
	    
	    UserLike::create(array(
		    'from_user_id' => $from,
		    'to_user_id' => $to
	    ));
	    
	    return 'OK';
    }
	
	public function likeTeam(Request $request) {
		$data = $request->all();
		
		$from = $data["user_id"];
		$to = $data["team_id"];
		
		$like = TeamLike::where(array(
			'user_id' => $from,
			'team_id' => $to
		))->get();
		
		if(count($like) > 0) {
			throw new \Exception("You can't like a team twice");
		}
		
		TeamLike::create(array(
			'user_id' => $from,
			'team_id' => $to
		));
		
		return 'OK';
	}
}
