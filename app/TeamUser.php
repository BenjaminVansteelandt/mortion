<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class TeamUser extends Model
{
	protected $fillable = [ 'team_id', 'user_id' ];
	
	public function user() {
		return $this->hasOne('App\User', 'id');
	}
	
	public function team() {
		return $this->hasOne('App\Team', 'id');
	}
}
