<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class TeamLike extends Model
{
    protected $fillable = array(
    	'team_id',
	    'user_id'
    );
}
