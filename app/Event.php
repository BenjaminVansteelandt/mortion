<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Event extends Model
{
    protected $fillable = [ 'name', 'description', 'time'];
	
	public function teams() {
		return $this->hasMany('App\Team');
	}
}
