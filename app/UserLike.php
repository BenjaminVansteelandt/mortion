<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class UserLike extends Model
{
    protected $fillable = [ 'from_user_id', 'to_user_id' ];
	
	public function users() {
		$this->hasMany('App\User');
	}
}
