<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class EventTeam extends Model
{
    protected $fillable = [ 'event_id', 'team_id' ];
	
	public function event() {
		return $this->belongsTo('App\Event', 'event_id');
	}
	
	public function team() {
		return $this->belongsTo('App\Team', 'team_id');
	}
}
