<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddForeignKeysToTeamLikes extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('team_likes', function (Blueprint $table) {
	        $table->foreign('user_id')->references('id')->on('users');
	        $table->foreign('team_id')->references('id')->on('teams');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('team_likes', function (Blueprint $table) {
            $table->dropForeign('team_likes_user_id_foreign');
            $table->dropForeign('team_likes_team_id_foreign');
        });
    }
}
