<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddForeignKeysToEventTeams extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('event_teams', function (Blueprint $table) {
	        $table->foreign('event_id')->references('id')->on('events');
	        $table->foreign('team_id')->references('id')->on('teams');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('event_teams', function (Blueprint $table) {
            $table->dropForeign('event_teams_event_id_foreign');
            $table->dropForeign('event_teams_team_id_foreign');
        });
    }
}
