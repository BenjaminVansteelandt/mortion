<?php

use Illuminate\Database\Seeder;

class UsersSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        \App\User::create(array(
        	'name' => 'Benjamin Vansteelandt',
	        'email' => 'benjaminvansteelandt@hotmail.com',
	        'password' => bcrypt('123456'),
	        'role_id' => 1
        ));
    }
}
