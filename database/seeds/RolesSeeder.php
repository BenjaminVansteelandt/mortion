<?php

use Illuminate\Database\Seeder;

class RolesSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        \App\Role::create(array(
        	'name' => 'visitor'
        ));
	
	    \App\Role::create(array(
		    'name' => 'e-sporter'
	    ));
    }
}
